class PhonesService {
  getPhones ({ query, orderBy } = {}) {
    return this._sendRequest('http://localhost:3000/phones/phones.json')
      .then((response) => {
        const filteredPhones = this._search(query, response);
        return this._sort(orderBy, filteredPhones);
      });
  }

  getPhoneByID (phoneID) {
    return this._sendRequest(`http://localhost:3000/phones/${phoneID}.json`, 'POST');
  }

  _sendRequest (url, method = 'GET') {
    // eslint-disable-next-line promise/param-names
    // return new Promise((res, rej) => {
    //   // eslint-disable-next-line no-undef
    //   const xhr = new XMLHttpRequest();
    //   xhr.open(method, url);
    //   xhr.onload = () => {
    //     if (!xhr.status === 200) {
    //       rej(xhr.statusText);
    //     }
    //     res(JSON.parse(xhr.responseText));
    //   };
    //   xhr.send();
    // });
    const options = {
      method
    };
    // eslint-disable-next-line no-undef
    return fetch(url, options)
      .then((response) => response.json());
  }

  _search (query, phones) {
    if (!query) {
      return phones;
    }
    return phones.filter((phone) => phone.name.toLowerCase().includes(query.toLowerCase()));
  }

  _sort (orderBy = 'name', phones) {
    if (!orderBy) {
      return phones;
    }
    phones.sort((phone1, phone2) => {
      if (phone1[orderBy] > phone2[orderBy]) {
        return 1;
      }
      if (phone1[orderBy] < phone2[orderBy]) {
        return -1;
      }
      return 0;
    });
    return phones;
  }
};

export const phonesService = new PhonesService();
