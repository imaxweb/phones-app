import { BaseComponent } from './../shared/base.component.js';

export class FiltersComponent extends BaseComponent {
  constructor ({ element }) {
    super({ element });
    this._render();

    this
      .on('input', '.search-input', ({ delegatedTarget: { value } }) => {
        this.emit('on-search', value);
      })
      .on('change', '.sort-by', ({ delegatedTarget: { value } }) => {
        this.emit('on-order', value);
      });
  }

  _render () {
    this._element.innerHTML = `
      <p>
        Search:
        <input type="text" class="search-input">
      </p>

      <p>
        Sort by:
        <select class="sort-by">
          <option value="name">Alphabetical</option>
          <option value="age">Newest</option>
        </select>
      </p>
    `;
  }
}
