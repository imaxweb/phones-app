import { BaseComponent } from '../shared/base.component.js';

export class PhoneDetailsComponent extends BaseComponent {
  constructor ({ element }) {
    super({ element });

    this
      .on('click', '.btn--back', ({ delegatedTarget }) => {
        this.emit('on-back', delegatedTarget);
      })
      .on('click', '.thumb', ({ delegatedTarget }) => {
        this._currentImg.src = delegatedTarget.src;
      })
      .on('click', '.btn--add', () => {
        this.emit('add-to-cart', this._phone.id);
      });
  }

  show (phone) {
    this._phone = phone;
    this._render();

    this._currentImg = this._element.querySelector('.phone');
    [this._currentImg.src] = this._phone.images;

    super.show();
  }

  _render () {
    this._element.innerHTML = `
    <div>

    <img class="phone">

    <button class="btn btn--back">Back</button>
    <button class="btn btn--add">Add to basket</button>


    <h1>${this._phone.name}</h1>

    <p>${this._phone.description}</p>

    <ul class="phone-thumbs">
      ${this._phone.images.map((src) => `
        <li>
          <img src="${src}" class="thumb">
        </li>
      `).join('')}
    </ul>
  </div>
    `;
  }
}
