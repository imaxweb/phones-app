import { phonesService } from './services/phones.service.js';
import { BaseComponent } from './shared/base.component.js';
import { PhonesCatalogComponent } from './phones-catalog/phones-catalog.component.js';
import { PhoneDetailsComponent } from './phone-details/phone-details.component.js';
import { CartComponent } from './cart/cart.component.js';
import { FiltersComponent } from './filters/filters.component.js';

export class App extends BaseComponent {
  constructor ({ element }) {
    super({ element });
    this._render();
    this._initCatalog();
    this._initDetails();
    this._initCart();
    this._initFilters();
  }

  _initCatalog () {
    this._catalog = new PhonesCatalogComponent({
      element: this._element.querySelector('.phones-catalog')
    });

    this._showFilteredPhones();

    this._catalog
      .subscribe('on-select', async ({ detail: phoneID }) => {
        try {
          const phone = await phonesService.getPhoneByID(phoneID);
          this._catalog.hide();
          this._details.show(phone);
        } catch (err) {
          console.log(err);
        }
      })
      .subscribe('add-to-cart', ({ detail: phoneID }) => {
        this._cart.add(phoneID);
      });
  }

  _initDetails () {
    this._details = new PhoneDetailsComponent({
      element: this._element.querySelector('.phone-details')
    });

    this._details
      .subscribe('on-back', () => {
        this._showFilteredPhones();
        this._details.hide();
      })
      .subscribe('add-to-cart', ({ detail: phoneID }) => {
        this._cart.add(phoneID);
      });
  }

  _initCart () {
    this._cart = new CartComponent({
      element: this._element.querySelector('.cart')
    });
  }

  _initFilters () {
    this._filters = new FiltersComponent({
      element: this._element.querySelector('.filters')
    });

    this._filters
      .subscribe('on-search', ({ detail: searchText }) => {
        this._query = searchText;
        this._showFilteredPhones();
      })
      .subscribe('on-order', ({ detail: orderBy }) => {
        this._orderBy = orderBy;
        this._showFilteredPhones();
      });
  }

  async _showFilteredPhones () {
    const phones = await phonesService.getPhones({ query: this._query, orderBy: this._orderBy });
    this._catalog.show(phones);
  }

  _render () {
    this._element.innerHTML = `
    <div class="container-fluid">
    <div class="row">

      <!--Sidebar-->
      <div class="col-md-2">
        <section class="filters"></section>

        <section class="cart"></section>
      </div>

      <!--Main content-->
      <div class="col-md-10 phones-catalog"></div>
      <div class="col-md-10 phone-details"></div>
    </div>
  </div>
    `;
  }
}
