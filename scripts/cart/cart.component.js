import { BaseComponent } from '../shared/base.component.js';

export class CartComponent extends BaseComponent {
  constructor ({ element }) {
    super({ element });
    this._phones = {};
    this._render();

    this.on('click', '.btn--remove', (e) => {
      const phoneID = e.target.closest('.cart-item').dataset.phoneId;

      this._phones[phoneID] -= 1;
      if (this._phones[phoneID] === 0) {
        delete this._phones[phoneID];
      }
      this._render();
    });
  }

  add (phoneID) {
    if (!this._phones[phoneID]) {
      this._phones[phoneID] = 1;
      this._render();
      return;
    }
    this._phones[phoneID] += 1;
    this._render();
  }

  _render () {
    this._element.innerHTML = `
      <p>Shopping Cart</p>
      <ul>
        ${Object.entries(this._phones).map(([id, count]) => `
          <li class="cart-item" data-phone-id="${id}">
           <span>${id} - ${count}шт.</span>
           <button class="btn btn--remove">x</button>
          </li>
        `).join('')}  
      </ul>
    `;
  }
}
