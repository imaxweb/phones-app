import { BaseComponent } from '../shared/base.component.js';

export class PhonesCatalogComponent extends BaseComponent {
  constructor ({ element }) {
    super({ element });
    this._phones = [];
    this._render();

    this
      .on('click', '.thumbnail', (e) => {
        if (!e.target.classList.contains('btn-success')) {
          this.emit('on-select', e.delegatedTarget.dataset.phoneId);
        }
      })
      .on('click', '.btn-success', ({ target }) => {
        const phoneID = target.closest('.thumbnail').dataset.phoneId;
        this.emit('add-to-cart', phoneID);
      });
  }

  show (phones) {
    this._phones = phones;
    this._render();
    super.show();
  }

  _render () {
    this._element.innerHTML = `
    <ul class="phones">
      ${this._phones.map((phone) => `
        <li class="thumbnail" data-phone-id="${phone.id}">
          <a href="#!/phones/${phone.id}" class="thumb">
            <img alt="${phone.name}" src="${phone.imageUrl}">
          </a>

          <div class="phones__btn-buy-wrapper">
            <a class="btn btn-success">
              Add
            </a>
          </div>

          <a href="#!/phones/${phone.id}">${phone.name}</a>
          <p>${phone.snippet}</p>
        </li>
      `).join('')}      
    </ul>
    `;
  }
}
