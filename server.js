console.log('Start server');

const http = require('http');
const staticServer = require('node-static');
const server = new staticServer.Server(
  `${__dirname}/`,
  {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*'
    }
  }
);

http.createServer((req, res) => {
  server.serve(req, res);
}).listen(3000);
